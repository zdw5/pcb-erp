package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 拓展属性值对象 erp_attribute_item
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpAttributeItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 属性ID */
    @Excel(name = "属性ID")
    private String attributeId;

    /** 属性label */
    @Excel(name = "属性label")
    private String attributeLabel;

    /** 属性值 */
    @Excel(name = "属性值")
    private String attributeValue;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private String useState;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setAttributeId(String attributeId) 
    {
        this.attributeId = attributeId;
    }

    public String getAttributeId() 
    {
        return attributeId;
    }
    public void setAttributeLabel(String attributeLabel) 
    {
        this.attributeLabel = attributeLabel;
    }

    public String getAttributeLabel() 
    {
        return attributeLabel;
    }
    public void setAttributeValue(String attributeValue) 
    {
        this.attributeValue = attributeValue;
    }

    public String getAttributeValue() 
    {
        return attributeValue;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setUseState(String useState) 
    {
        this.useState = useState;
    }

    public String getUseState() 
    {
        return useState;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("attributeId", getAttributeId())
            .append("attributeLabel", getAttributeLabel())
            .append("attributeValue", getAttributeValue())
            .append("sort", getSort())
            .append("useState", getUseState())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
