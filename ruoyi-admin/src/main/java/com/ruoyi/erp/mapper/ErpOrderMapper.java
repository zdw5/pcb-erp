package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpOrder;

/**
 * 订单Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public ErpOrder selectErpOrderById(String id);

    /**
     * 查询订单列表
     * 
     * @param erpOrder 订单
     * @return 订单集合
     */
    public List<ErpOrder> selectErpOrderList(ErpOrder erpOrder);

    /**
     * 新增订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    public int insertErpOrder(ErpOrder erpOrder);

    /**
     * 修改订单
     * 
     * @param erpOrder 订单
     * @return 结果
     */
    public int updateErpOrder(ErpOrder erpOrder);

    /**
     * 删除订单
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteErpOrderById(String id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpOrderByIds(String[] ids);
}
