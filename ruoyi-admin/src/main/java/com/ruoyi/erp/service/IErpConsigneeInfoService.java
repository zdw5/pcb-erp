package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpConsigneeInfo;

/**
 * 常用收货地址Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpConsigneeInfoService 
{
    /**
     * 查询常用收货地址
     * 
     * @param id 常用收货地址ID
     * @return 常用收货地址
     */
    public ErpConsigneeInfo selectErpConsigneeInfoById(String id);

    /**
     * 查询常用收货地址列表
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 常用收货地址集合
     */
    public List<ErpConsigneeInfo> selectErpConsigneeInfoList(ErpConsigneeInfo erpConsigneeInfo);

    /**
     * 新增常用收货地址
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 结果
     */
    public int insertErpConsigneeInfo(ErpConsigneeInfo erpConsigneeInfo);

    /**
     * 修改常用收货地址
     * 
     * @param erpConsigneeInfo 常用收货地址
     * @return 结果
     */
    public int updateErpConsigneeInfo(ErpConsigneeInfo erpConsigneeInfo);

    /**
     * 批量删除常用收货地址
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpConsigneeInfoByIds(String ids);

    /**
     * 删除常用收货地址信息
     * 
     * @param id 常用收货地址ID
     * @return 结果
     */
    public int deleteErpConsigneeInfoById(String id);
}
