package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpStockInLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpStockOutLog;
import com.ruoyi.erp.service.IErpStockOutLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 出库日志Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpStockOutLog")
public class ErpStockOutLogController extends BaseController
{
    private String prefix = "erp/erpStockOutLog";

    @Autowired
    private IErpStockOutLogService erpStockOutLogService;

    @RequiresPermissions("erp:erpStockOutLog:view")
    @GetMapping()
    public String erpStockOutLog()
    {
        return prefix + "/erpStockOutLog";
    }

    /**
     * 查询出库日志列表
     */
    @RequiresPermissions("erp:erpStockOutLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpStockOutLog erpStockOutLog)
    {
        startPage();
        List<ErpStockOutLog> list = erpStockOutLogService.selectErpStockOutLogList(erpStockOutLog);
        return getDataTable(list);
    }

    /**
     * 导出出库日志列表
     */
    @RequiresPermissions("erp:erpStockOutLog:export")
    @Log(title = "出库日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpStockOutLog erpStockOutLog)
    {
        List<ErpStockOutLog> list = erpStockOutLogService.selectErpStockOutLogList(erpStockOutLog);
        ExcelUtil<ErpStockOutLog> util = new ExcelUtil<ErpStockOutLog>(ErpStockOutLog.class);
        return util.exportExcel(list, "erpStockOutLog");
    }

    /**
     * 新增出库日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存出库日志
     */
    @RequiresPermissions("erp:erpStockOutLog:add")
    @Log(title = "出库日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpStockOutLog erpStockOutLog)
    {
        return toAjax(erpStockOutLogService.insertErpStockOutLog(erpStockOutLog));
    }

    /**
     * 修改出库日志
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpStockOutLog erpStockOutLog = erpStockOutLogService.selectErpStockOutLogById(id);
        mmap.put("erpStockOutLog", erpStockOutLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存出库日志
     */
    @RequiresPermissions("erp:erpStockOutLog:edit")
    @Log(title = "出库日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpStockOutLog erpStockOutLog)
    {
        return toAjax(erpStockOutLogService.updateErpStockOutLog(erpStockOutLog));
    }

    /**
     * 删除出库日志
     */
    @RequiresPermissions("erp:erpStockOutLog:remove")
    @Log(title = "出库日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpStockOutLogService.deleteErpStockOutLogByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpStockOutLog erpStockOutLog = erpStockOutLogService.selectErpStockOutLogById(id);
        mmap.put("erpStockOutLog", erpStockOutLog);
        return prefix + "/detail";
    }
}
