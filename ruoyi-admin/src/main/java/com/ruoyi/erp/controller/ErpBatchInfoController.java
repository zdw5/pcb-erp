package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpAfterFeedback;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpBatchInfo;
import com.ruoyi.erp.service.IErpBatchInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产批次Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpBatchInfo")
public class ErpBatchInfoController extends BaseController
{
    private String prefix = "erp/erpBatchInfo";

    @Autowired
    private IErpBatchInfoService erpBatchInfoService;

    @RequiresPermissions("erp:erpBatchInfo:view")
    @GetMapping()
    public String erpBatchInfo()
    {
        return prefix + "/erpBatchInfo";
    }

    /**
     * 查询生产批次列表
     */
    @RequiresPermissions("erp:erpBatchInfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpBatchInfo erpBatchInfo)
    {
        startPage();
        List<ErpBatchInfo> list = erpBatchInfoService.selectErpBatchInfoList(erpBatchInfo);
        return getDataTable(list);
    }

    /**
     * 导出生产批次列表
     */
    @RequiresPermissions("erp:erpBatchInfo:export")
    @Log(title = "生产批次", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpBatchInfo erpBatchInfo)
    {
        List<ErpBatchInfo> list = erpBatchInfoService.selectErpBatchInfoList(erpBatchInfo);
        ExcelUtil<ErpBatchInfo> util = new ExcelUtil<ErpBatchInfo>(ErpBatchInfo.class);
        return util.exportExcel(list, "erpBatchInfo");
    }

    /**
     * 新增生产批次
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存生产批次
     */
    @RequiresPermissions("erp:erpBatchInfo:add")
    @Log(title = "生产批次", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpBatchInfo erpBatchInfo)
    {
        return toAjax(erpBatchInfoService.insertErpBatchInfo(erpBatchInfo));
    }

    /**
     * 修改生产批次
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpBatchInfo erpBatchInfo = erpBatchInfoService.selectErpBatchInfoById(id);
        mmap.put("erpBatchInfo", erpBatchInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存生产批次
     */
    @RequiresPermissions("erp:erpBatchInfo:edit")
    @Log(title = "生产批次", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpBatchInfo erpBatchInfo)
    {
        return toAjax(erpBatchInfoService.updateErpBatchInfo(erpBatchInfo));
    }

    /**
     * 删除生产批次
     */
    @RequiresPermissions("erp:erpBatchInfo:remove")
    @Log(title = "生产批次", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpBatchInfoService.deleteErpBatchInfoByIds(ids));
    }

    @RequiresPermissions("erp:erpBatchInfo:view")
    @GetMapping("/pageSelectBatch")
    public String pageSelectBatch() {
        return prefix + "/pageSelectBatch";
    }

    @RequiresPermissions("erp:erpBatchInfo:view")
    @GetMapping("/wipView")
    public String wipView() {
        return prefix + "/wipView";
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpBatchInfo erpBatchInfo = erpBatchInfoService.selectErpBatchInfoById(id);
        mmap.put("erpBatchInfo", erpBatchInfo);
        return prefix + "/detail";
    }
}
